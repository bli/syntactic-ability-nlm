This directory contains the predictions of the best LSTM model and Transformer model and useful statistics of sentences. The `*.pred` file contains the following tab-delimited fields: 
1. `pattern`  construction (defined by the sequence of PoS tags) the current sentence instantiates: `obj_aux_PP` annotate the French object past participle agreement construction.

2. `constr_id`  id of the reference original sentence

4. `sent_id`  id for the specific sentence, 0 corresponds to the original reference sentence, 1-3 to nonce generated sentence 

4) `type`  whether the sentence is original or generated (that is, nonce)

5) `class`  whether the target is correct or wrong

6) `form`  candidate target

7) `correct_number`  whether the correct target should be sing or plur

8) `cls_noun_num`  the number of the left closest noun of the target verb 

9) `cls_token_num`  the number of the left closest token with number feature of the target verb

10) `fst_noun_num`  the number of the first noun of the sentence 

11) `com_num`  the majority number expressed in the prefix 

12) `que_n_num`  the number of the first noun on the left of the object `que`  

13) `que_id` the id of the object `que` in the sentence 

14) `n_attr`  number of attractors 

15) `freq`  frequency of target form in training corpus

16) `prefix`  the sentence up to and excluding the target

17) `len_prefix`  number of words from beginning of sentence up to and excluding target 

18) `len_context`  number of words in context

19) `inverted_subj` whether the subject and verb of the relative clause is inverted

20) `sent`  the whole sentence feed into language model

21) `prob`  probability assigned to this target by best pre-trained models


