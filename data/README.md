## Data for model training and syntacitc evaluation
- `agreement` contains evaluation test sets, some features of these sentences for data analysis (e.g. prefix, context length, number of the first noun in prefix, etc. ) and the automatically-parsed Gutenberg corpora using [FlauBERT](https://arxiv.org/abs/1912.05372) and [spaCy](https://spacy.io/)
- `lm` contains language models' training data and the best pre-trained language models
- `evaluation_results` contains the prediction of our trained models on French object-verb agreement task. 


