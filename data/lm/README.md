### Training data based on French Wikipedia
We extracted 100M tokens from French Wikipedia dump(2020-11-09) and split it into training, validation and test sets with a standard 8:1:1 proportion. The corpus has been segmented and tokenized with [Moses tokenizer](https://aclanthology.org/P07-2045.pdf) and we filtered out sentences with more than 5% unknown tokens. We bound the bocabulary of LMs to the 50k most frequent tokens found in the training data, `<unk>`,`<pad>`, `<bos` and `<eos>` tokens are already in the vocabulary.



### Best Pre-trained language models
The models were trained with data sets given above. We distribute the best LSTM and Transformer models in terms of perplexity(the lowest) on the validation set. 
- [LSTM model](../../src/lm/LSTM)
- [Transformer model](../../src/lm/TM)
