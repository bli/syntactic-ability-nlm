# -*- coding: utf-8 -*-
import argparse
import json
from collections import defaultdict
import depTree as dt


parser = argparse.ArgumentParser(description='Extracting word forms, lemmas and morphological features of a treebank(.conll)')

parser.add_argument('--input', type=str, required=True,
                    help='Input file (in a column CONLL UD format)')
parser.add_argument('--output', type=str, required=True, help="Output file prefix")
parser.add_argument('--nwords', type=int, default='100000000', required=False,
                    help='How many words to process')
parser.add_argument('--min_freq', type=int, default='5', required=False,
                    help='Minimal frequency of paradigm to be included in the dictionary')
args = parser.parse_args()


nwords = 0
paradigms = defaultdict(int)
paradigms_avoir_verb = defaultdict(int)

stream = open(args.input)
for line in stream:
    # if empty line or # line (not conll line)
    if line.strip() == "" or len(line.split("\t")) < 2:
        continue
    else:
        fields = line.split("\t")
        # ignore the punctuation line
        if fields[1].isalpha():
            morph = fields[5]
            wordform = fields[1]
            #  e.g."dénoncée" not in paradigms because of "Voice=Pass"
            if fields[3] in ["VERB","ADJ"]:
                wordform=wordform.lower()
                if "Voice=Pass" in morph:
                    morph = morph.replace("|Voice=Pass","")
            #          wordform   lemma       Pos        morpho-syntac
            paradigms[(wordform, fields[2], fields[3], morph)] += 1
        nwords += 1

    if nwords > args.nwords:
        print("nwords > args.nwords")
        break 
stream.close()

'''extract word forms, lemmas and morphological features to be included in the dictonary'''
c = 0
with open(args.output + "_paradigms.txt", 'w') as f:
    for p in paradigms:
        if paradigms[p] > args.min_freq:
            f.write("\t".join(el for el in p) + "\t" + str(paradigms[p]) + "\n")
            c+=1
    f.close()

print("paradigms collection finished, nwords = ", c)
print("paradigms stocked in local: ",args.output+"_paradigms.txt")



"""extract verbs using 'avoir' as auxiliary"""

# trees is a list of DependencyTree objets
trees = dt.load_trees_from_conll(args.input)
avoir_verbs  = set()
for t in trees:
    for a in t.arcs:
        # verbs using "avoir" as auxiliary
        if a.head.pos == "VERB" and a.dep_label == "aux:tense" and a.child.lemma == "avoir":
            v_child_dep = [n.dep_label for n in t.nodes if n.head_id == a.head.index]
            # verbs have direct object (SVO or passive e.g. le billet a été bien écrit)
            if "obj" in v_child_dep or "nsubj:pass" in v_child_dep:
                morph = a.head.morph
                wordform = a.head.word.lower()
                if "Voice=Pass" in morph:
                    morph = morph.replace("|Voice=Pass", "")
                paradigms_avoir_verb[(wordform, a.head.lemma,morph)] += 1
with open(args.output+"_avoir_verbs_paradigms.txt","w") as f:
    for p in paradigms_avoir_verb:
        avoir_verbs.add(p[1])
        f.write("\t".join(el for el in p) + "\t" + str(paradigms_avoir_verb[p]) + "\n")
print("TR avoir verbs paradigms stocked, nb of verb forms= ",len(paradigms_avoir_verb.keys()))

"""store the lemmas of these verbs for the generating process """
with open(args.output + "_avoir_verbs_lemmas.json", "w") as file:
    json.dump(list(avoir_verbs),file)
print("TR avoir verbs lemma list stocked, nb of lemmas: ", len(list(avoir_verbs)))
