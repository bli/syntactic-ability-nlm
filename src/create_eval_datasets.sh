#!/usr/bin/env bash
#
# Copyright (c) 2009-present  CNRS
# All rights reserved.
#
# This source code is licensed under the license found in the
# COPYING file in the root directory of this source tree.
#

#!/bin/bash

# path to a treebank
# extracted Gutenberg corpus has been automatically parsed using FlauBERT and spaCy
treebank="../data/agreement/gutenberg-treebank.conllu"


# path to the training LM data, including vocab.txt, e.g.
lm_data="../data/lm/"

mkdir -p tmp/French

# extracting word forms, lemmas and morphological features
echo "== collect paradigms =="
#python create_testsets/collect_paradigms.py --input $treebank --output tmp/French/fr --min_freq 2

echo "== extract agreement constructions and generate test set =="
# extracting constructions which correspond to agreement relation, grep corresponding instances which satisfy generation conditions 
python create_testsets/generate_agreement_testset.py --treebank $treebank --paradigms tmp/$lang/paradigms.txt \
                                               --vocab $lm_data/vocab.txt --patterns tmp/$lang/patterns.txt \
                                               --output ../data/agreement/$lang/generated \
                                               --lm_data $lm_data   # for estimation of token probabilities from LM training data


