# LSTM and Transformer-based language models

This directory contains scripts for training the LSTM and Transformer language models, based on the repository `Lm4Ling` of Benoît Crabbé. It implements word-based language models and returns word based 
probability transition statistics. It implements RNN, LSTM and Transformer style architectures.



Evaluating a language model on testing data
--------

For each architecture, we distribute the trained model that achieved the lowest perplexity on our validation set. The best pre-trained models are stored in their `MODEL_DIR` directory: [LSTM](LSTM)(perplexity=36.8) and [TM](TM)(perplexity=28.2). You can download these models to perform predictions on the testing data. The data must be a tokenized, one-sentence-per-line text file(white space indicates token boundaries).

Please refer to the notebook [Evaluation.ipynb](Evaluation.ipynb) for guidelines on how to use pretrained models to perform agreement tasks.

Here is an example for performing predictions on a two-sentence toy test file whose `MODEL_DIR`is `TM` using `gpu 0`:
``` shell script
python nnlm.py TM --test_file sent2_toy_test.txt --device_name cuda:0 
```
The prediction returns log probabilities each sentence in the test file. Here is an example for a pair of short sentences:

```
            token       ref_next     pred_next  ref_prob  pred_prob
0           <bos>            Les            Il -2.762850  -2.177223
1             Les         offres         <unk> -9.470440  -2.971430
2          offres            que            de -7.334555  -1.151693
3             que             le            le -1.795184  -1.795184
4              le      directeur  gouvernement -6.070398  -1.644881
5       directeur              a            de -3.913091  -0.752605
6               a      acceptées        faites -5.907149  -0.782099
7       acceptées           sont          sont -1.749241  -1.749241
8            sont  intéressantes         <unk> -8.084695  -3.179098
9   intéressantes              .             . -0.881322  -0.881322
10              .          <eos>         <eos> -0.002251  -0.002251

            token       ref_next     pred_next  ref_prob  pred_prob
0           <bos>            Les            Il -2.762850  -2.177223
1             Les         offres         <unk> -9.470440  -2.971430
2          offres            que            de -7.334555  -1.151693
3             que             le            le -1.795184  -1.795184
4              le      directeur  gouvernement -6.070398  -1.644881
5       directeur              a            de -3.913091  -0.752605
6               a       acceptée        faites -8.258838  -0.782099
7        acceptée           sont          sont -1.982919  -1.982919
8            sont  intéressantes         <unk> -8.100102  -2.907381
9   intéressantes              .             . -0.851921  -0.851921
10              .          <eos>         <eos> -0.002212  -0.002212
```

Training your own model
--------
 
 The trained model will be stored in a directory called `MODEL_DIR`. 
 The first thing to do is to create it:
``` shell script
mkdir MODEL_DIR
cd MODEL_DIR
```
Inside the model directory, you have to create a file called `model.yaml` 
that contains the range of hyperparameters you want to search for
during training. The [TM](TM) or [LSTM](LSTM) directory in this git provides examples of such a directory
with hyperparameters for training a standard LSTM/TM model

Once created and assuming that `MODEL_DIR` is currently 
in your current working directory, the training procedure is launched 
as follows:
``` shell script
python nnlm.py MODEL_DIR --train_file ../../data/lm/train.txt --valid_file ../../data/lm/valid.txt --device_name cuda:0  
``` 
where we use the sampled train and validation files in `../../data/lm/` and we specify that the computations will take place 
on `gpu 0`. In case no device is specified, it defaults to `cpu` which is not the recommended usage for training

**Grid Search** for training the `model.yaml` may contain lists of values for some hyperparameters.
In which case, the trainer will perform a grid search by testing all 
the hyperparameter combinations. The `model_search.yaml` is an example of such 
a file. 

**Memory management** Training language models may result in memory blowup.
In case you get a `Runtime Error` with a memory problem. Try to 
reduce the value of the `batch_size` and `bptt_chunk` that represent the number of sequences in 
a batch and the maximum length of the sub-sequence processed by the truncated backpropagation through time algorithm. 
