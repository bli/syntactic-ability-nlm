This directory contains the scripts that we used to train LMs and run our experiments.
- `lm` contains scripts for training the LSTM and Transformer language models, based on the code: [https://github.com/pytorch/examples/tree/master/word_language_model](https://github.com/pytorch/examples/tree/master/word_language_model)
- `create_testsets`contains scripts to extract target reference sentences and generate sentences for the controlled experiments. 
    - check `create_eval_datasets.sh` to see how these scripts are used and put together 
